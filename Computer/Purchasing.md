# How to securely purchase hardware

## Some Base rules
- Never log in to "secure accounts" from your ip, or a service (vpn, proxy, etc) willing to coorporate with lettersoup
- Only purchase with cash
- Never buy more than 1 hardware item with a link back to the same identity (names, accounts, phone numbers)
- Destroy any serial numbers from devices after purchase
- Never buy hardware in a store with cctv
- Remove all power sources from device before leaving premise (power off is not enough)
- immediately discard any persistent storage from device (destruction not necessary)
- physically destroy wireless hardware in device (remove antennas, cut traces to wireless chipset if needed)
- try to source parts as far away from each other as possible (multiple jurisdictions if possible)



## how to cycle identities
The most important part when buying hardware through anything online is to cycle your identity between purchases. 
so it is harder to correlate your purchases back to you. This part is the hardest and requires proper discipline.

### Always use random fake information
This one is obvious. but there is some finesse to it. You want to programmatically generate this information to prevent possible data leakage 
(the human mind can have a bias to use information not really random and still leak data, E.G mashed up names we know, or repeated fake names)

In order to do this We will use an offline fake data generator that is pretty popular, [Faker](https://github.com/marak/Faker.js/)


### Use a tempmail 
This one is pretty obvious too, you should always use tempmail to register to any site where you will acquire hardware from. a good site to do this will be [tempmail](https://temp-mail.org/en/).
Only use this service trough TOR. 

### Never use the same identity on more than 1 site
This is to prevent the possibility of correlating your behaviour across multiple websites.



## How to pickup hardware

### Corona specific
Since we live in a global pandemic, we have some leeway in what is considered "normal", wearing a mask and gloves will not trigger any red flags. So I will highly encourage it. 
This will prevent possible ring doorbell or personal cctv to clearly capture your face. bright infrared leds can also be used to mess with security camera systems

### Preperation
Do not bring any of your electronic devices to the place you will pick up the hardware. Not even powered off
Any device could be compromised and log gps data, or ssid logging. even when not connected this data 
COULD be collected. the only safebet is to not bring them

### Clothing
Try to wear clothing with as little identifiers as possible.
- No Logos on clothes, no identifiable prints/marks/anything
- Dress down (on the lower end of fitting in, use your common sense with this one)
- Have a second jacket/ headwear to switch to, to make cctv correlation harder
- wear as much black as possible (obscures body type) without standing out

### Time slots
The timing of your pickup is very important, you want it to be when there is close to no one on the street, the evenings (7pm - 10pm) and morning (10am-13pm) will work well for that. 
but can differ per region so do your research.

### Don't use trackable transportation
Using cars that can be linked back to you is a big no-no. Always try to use something that can not be easily tracked (licence plate scanners exist).
public transport can be used but beware to only pay cash or anonymous, and have a provable alibi ready as to why you needed to go there (never go directly, walk the last miles) or take a cash cab



## How to cleanse items purchased

### Remove power as soon as possible
Turning of said hw will not be enough, remove the battery before leaving the premises where you purchased it. 

### Destroy any identifiable information about device
This includes serial codes, windows stickers, bios configuration (remove cmos battery)

### Remove any possible recording/communication devices
After you have done the bare essentials you should open up the device and physically destroy the following components if applicable
- Webcam, this should not be disconnected but destroyed
- Microphone, this can be located by the pinhole on the device , and with a screwdriver can be physically removed from the pcb
- wireless card: usually this will be a standalone component that can be removed, but if not it is best to look up the schematic of the device and cut the pcb traces to that device
- antennas, they should be physically removed from device

### Destroy any persistent data from device
You should permanently destroy any hard drives or ssds that came with the device. this is to prevent leakage of previous owners' data. which could lead back to your purchase.
If this is not possible to do, the device will not be able to be used.

