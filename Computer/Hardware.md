#WIP

# Computer hardware less compromised

## General philosophy
Hardware can be used to identify you. whether trough compromised hardware or simply by observing your unique combination of hardware.
It is therefore very important to have hardware that blends in as much as possible. 
In this case it is definitely important to be a sheep and try to be in the largest group of 
people with similar stuff without using compromised HW.

An example would be if you overclock your hardware, this is something that can be detected easily if your system 
is compromised and can be used to identify your machine in particular

## Processors
As far as processors are concerned there are certain processors 
from both AMD and Intel that have unknown binary blobs and co-processors that are currently regarded as government sponsored spyware. 
Any processor with these "features" should be regarded as not usable if privacy is of concern.

intel and amd also have a plethora of vulnerabilities and "features" that inhibit proper privacy

### How to spot these undesirable processors? (intel)
- any processor made after 2008 is not desirable by default
- processors that have `Vpro` functionality are considered compromised
- 

### How to spot these processors? (amd)


### circumvent compromised random number generators
We all know we cannot trust the entropy created by commercially available processors 
[for example](https://en.wikipedia.org/wiki/Dual_EC_DRBG#Software_and_hardware_which_contained_the_possible_backdoor).

The best bet you have is to use what is known as a hardware entropy generator 
[Like this one {This is not an endorsement!}](https://tectrolabs.com/alpharng/) 
and use that to seed your cryptography


